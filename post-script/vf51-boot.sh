#!/bin/bash

DISKIMG=vf51-boot

function DBG() {
    echo "<<<" $1
}

# Find the unused loop device
DBG "Find the unused loop device"
LOOPDEVICE=$(losetup -f)

# Create a disk image in a raw format using dd
DBG "Create a disk image"
dd if=/dev/zero of=$DISKIMG.img bs=1M count=50

# Associate the disk image with the available loop device
DBG "Associate the disk"
losetup $LOOPDEVICE $DISKIMG.img

# Create a partitions
DBG "Create a partitions"
fdisk $LOOPDEVICE<<EOF
n
p
1

+10M
n
p
2


w
EOF

# Create a device map from the device partition table
DBG "Create a device map for loop device"
kpartx -a -v $LOOPDEVICE

# Format the disk with a ext4 filesystem
DBG "Format the disk"
LOOPDEV1=$(fdisk -l $LOOPDEVICE | grep ${LOOPDEVICE}p | awk '{print $1}' | head -n 1 | cut -d "/" -f 3)
LOOPDEV2=$(fdisk -l $LOOPDEVICE | grep ${LOOPDEVICE}p | awk '{print $1}' | tail -n 1 | cut -d "/" -f 3)
sleep 2  # Waiting for detecting devices
mkfs.ext4 -F -L "uImage" /dev/mapper/$LOOPDEV1
mkfs.ext4 -F -L "rootfs" /dev/mapper/$LOOPDEV2

# Mount the `uImage` partition and copy uImage, dtb, uboot's image
DBG "Mount the first partition & Copy the data"
mkdir -p /media/uImage
mount /dev/mapper/$LOOPDEV1 /media/uImage/
cp output/uImage \
   output/vf500-colibri-zkit-base.dtb \
   output/u-boot-nand.imx \
   output/u-boot-env.bin \
   /media/uImage/

# Mount the `rootfs` partiotion and unpack the rootfs contents
DBG "Mount the second partition & copy the data"
mkdir -p /media/rootfs
mount /dev/mapper/$LOOPDEV2 /media/rootfs/
tar -xf output/rootfs.tar.gz -C /media/rootfs/

# Unmount all the filesystem and disassociate the loopdevice
DBG "Umount all the filesystems"
umount /media/uImage 
umount /media/rootfs 
kpartx -d $LOOPDEVICE
losetup -d $LOOPDEVICE
rm -fr /media/uImage /mdeia/rootfs
