# Create uboot environment binary
# Ref:
#  * http://free-electrons.com/blog/mkenvimage-uboot-binary-env-generator/
#  * https://nnc3.com/mags/LJ_1994-2014/LJ/246/11725.html
echo "<<< Create uboot environment binary"
MKENVIMAGE=./buildroot-2017.02/output/host/usr/bin/mkenvimage
UBOOT_ENV_FILE=u-boot-env
CONFIG_ENV_SIZE=0x20000
${MKENVIMAGE} -s ${CONFIG_ENV_SIZE} -o output/${UBOOT_ENV_FILE}.bin post-script/${UBOOT_ENV_FILE}.txt
