#!/bin/bash

# This is a post-build script executed by Buildroot before creating the
# filesystem images

echo "tmpfs /dev tmpfs defaults,size=1m 0 0" >> \
output/target/etc/fstab

echo "tmpfs /root tmpfs defaults,size=10m 0 0" >> \
output/target/etc/fstab
