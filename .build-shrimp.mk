packages = patch file cpio unzip rsync bc git kpartx
packages-i386 =         \
        libc6:i386      \
        gcc-arm-linux:i386

before_install:
	sudo dpkg --add-architecture i386
	sudo echo $(ZILOGIC_APT_REPO) | sudo tee /etc/apt/sources.list.d/zilogic.list
	sudo apt-get update

install:
	sudo apt-get install -y --no-install-recommends build-essential
	sudo apt-get install -y --force-yes --no-install-recommends $(packages)
	sudo apt-get install -y --force-yes --no-install-recommends $(packages-i386)

script:
	make
