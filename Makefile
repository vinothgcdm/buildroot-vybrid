BUILDROOT_VER=2017.02
BUILDROOT_FILE=buildroot-$(BUILDROOT_VER)
BUILDROOT_URL=http://buildroot.uclibc.org/downloads/$(BUILDROOT_FILE).tar.gz
BUILDROOT_OUTPUT=output

all: before-run-buildroot run-buildroot after-run-buildroot

after-run-buildroot:
	mkdir -p $(BUILDROOT_OUTPUT)
	cp $(BUILDROOT_FILE)/output/images/uImage $(BUILDROOT_OUTPUT)/
	cp $(BUILDROOT_FILE)/output/images/rootfs.tar.gz $(BUILDROOT_OUTPUT)/
	cp $(BUILDROOT_FILE)/output/images/u-boot-nand.imx $(BUILDROOT_OUTPUT)/
	cp $(BUILDROOT_FILE)/output/images/vf500-colibri-zkit-base.dtb $(BUILDROOT_OUTPUT)/
	./post-script/mkenvimage.sh
	sudo ./post-script/vf51-boot.sh

run-buildroot:
	make -C $(BUILDROOT_FILE) all

before-run-buildroot: download-buildroot
	cp config/br_vybrid_defconfig $(BUILDROOT_FILE)/configs/vybrid_defconfig
	make -C $(BUILDROOT_FILE) vybrid_defconfig

download-buildroot:
	wget -c $(BUILDROOT_URL)
	tar -xf $(BUILDROOT_FILE).tar.gz
	patch -d $(BUILDROOT_FILE) -p1 < patch/br.diff

clean:
	rm -fr $(BUILDROOT_FILE)/
	rm -fr $(BUILDROOT_OUTPUT)/
	rm -fr $(BUILDROOT_FILE).tar.gz
