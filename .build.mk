packages = patch file cpio unzip rsync bc git kpartx libc6:i386

before_install:
	sudo dpkg --add-architecture i386
	sudo apt-get update

install:
	sudo apt-get install -y --force-yes --no-install-recommends $(packages)
	sudo apt-get install -y --force-yes --no-install-recommends build-essential

script:
	make
