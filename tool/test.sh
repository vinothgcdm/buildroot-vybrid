str=$(mount | grep $1)
if [ "$str" != "" ]
then
    echo "Unmounting device ..." ${1}
    sudo umount ${1}?*
fi

(
    echo o      # Create a new empty DOS partition table
    echo n      # Add a new partition
    echo p      # Primary partition
    echo 1      # Partition number
    echo        # First sector (Accept default: 1)
    echo +100M  # Last sector (Accept default: varies)
    echo n      # Add a new partition
    echo p      # Primary partition
    echo 2      # Partition number
    echo        # First sector (Accept default: 1)
    echo +100M  # Last sector (Accept default: varies)
    echo w      # Write changes
) | sudo fdisk ${1} &&
    sleep 4 &&
    sudo mkfs.ext4 -F -L "uImage" ${1}1 &&
    sudo mkfs.ext4 -F -L "rootfs" ${1}2
